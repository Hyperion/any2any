# any2any

any2any is a media converter tool based on ffmpeg. 
any2any can convert any audio or video format into any format, in single file or bulk "all files in directory" mode. 
any2any is multi-threaded : conversion of hundreds of files takes a few  minutes.

Dependecies :
- ffmpeg
- kid3-cli (optional)

Installation :
- Copy any2any in /usr/bin
- Link any2mp4, any2mp3,... to any2any (optional)

Usage : 
 any2any [--target mp3|ogg|...|target format] (will convert all media files in the current directory to the specified target format)
 any2any [--input MEDIA-FILE1 --input MEDIA-FILE2 --input ...] (will convert specified media files)
 any2mp3 (will convert all media files in the current directory to the mp3 transparent quality)
 any2flac (will convert all media files in the current directory to flac)
 any2opus (will convert all media files in the current directory to the opus transparent quality)
 any2ogg (will convert all media files in the current directory to the opus transparent quality)

